import java.util.Date;

public class Person {
    private String name;
    private String surname;
    private String patronymic;
    private int age;
    private boolean sex;
    private Date date;

    public Person(){
    }
    public Person(String name, String surname, String patronymic, int age, boolean sex, Date date){
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.sex = sex;
        this.date = date;
    }
    public Person(String name, String surname, int age, boolean sex, Date date){
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
        this.date = date;
    }
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getAge() {
        return age;
    }
    public boolean isSex() {
        return sex;
    }

    public Date getDate() {
        return date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
