import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public final class CreatePerson {
    public static List createPerson() {
        Person person1 = new Person("Иван", "Иванов", "Иванович", 23, true, Date.valueOf("12.12.1997"));
        Person person2 = new Person("Иван", "Петров", 19, true, Date.valueOf("12.12.1997"));
        Person person3 = new Person("Петр", "Сидоров", "Иванович", 18, true, Date.valueOf("12.12.1997"));
        Person person4 = new Person("Мария", "Андреева", "Сергеевна", 23, false, Date.valueOf("12.12.1997"));
        List<Person> listOfPerson = new ArrayList<Person>();
        listOfPerson.add(person1);
        listOfPerson.add(person2);
        listOfPerson.add(person3);
        listOfPerson.add(person4);
        return listOfPerson;
    }
}
